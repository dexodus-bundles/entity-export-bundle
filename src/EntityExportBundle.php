<?php

declare(strict_types=1);

namespace Dexodus\EntityExportBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class EntityExportBundle extends Bundle
{
}
