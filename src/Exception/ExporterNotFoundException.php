<?php

declare(strict_types=1);

namespace Dexodus\EntityExportBundle\Exception;

use Exception;

class ExporterNotFoundException extends Exception
{
}
