<?php

declare(strict_types=1);

namespace Dexodus\EntityExportBundle\Service\Exporter;

use DateTime;
use Dexodus\EntityTableBundle\Dto\EntityTableStructure;
use Dexodus\Jsel\Jsel;
use Dexodus\Jsel\JselContext;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class XlsxExporter implements ExporterInterface
{
    public function export(EntityTableStructure $entityTableStructure, array $entities = []): string
    {
        $spreadSheet = new Spreadsheet();
        $worksheet = $spreadSheet->getActiveSheet();

        $i = 1;

        foreach ($entityTableStructure->columns as $column) {
            $worksheet->setCellValue([$i++, 1], $column->title);
        }

        $y = 2;

        foreach ($entities as $entity) {
            $jsel = new Jsel(new JselContext(['entity' => $entity]));

            $i = 1;

            foreach ($entityTableStructure->columns as $column) {
                $worksheet->setCellValue([$i++, $y], $jsel->exec($column->getDataAction));
            }

            $y++;
        }

        $writer = new Xlsx($spreadSheet);

        $currentTime = (new DateTime())->format('Y-m-d_H:i:s');
        $exportedFilePath = "/tmp/exported_{$entityTableStructure->name}_$currentTime.xlsx";
        $writer->save($exportedFilePath);

        return $exportedFilePath;
    }
}
