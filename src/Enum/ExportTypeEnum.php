<?php

declare(strict_types=1);

namespace Dexodus\EntityExportBundle\Enum;

enum ExportTypeEnum: string
{
    case TYPE_XLSX = 'xlsx';
}
